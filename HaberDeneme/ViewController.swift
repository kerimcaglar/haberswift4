//
//  ViewController.swift
//  HaberDeneme
//
//  Created by Kerim on 7/27/17.
//  Copyright © 2017 Kerim. All rights reserved.
//

import UIKit
import AEXML
import Alamofire
import Auk

class ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        scrollView.auk.show(url: "https://bit.ly/auk_image")
        scrollView.auk.show(url: "https://bit.ly/moa_image")
        
        // Scroll images automatically with the interval of 3 seconds
        scrollView.auk.startAutoScroll(delaySeconds: 3)
        
        // Stop auto-scrolling of the images
        scrollView.auk.stopAutoScroll()
        
        Alamofire.request("http://www.trt.net.tr/rss/turkiye.rss").response { (haber) in
            guard let xml = haber.data else {return}
            
            var options = AEXMLOptions()
            options.parserSettings.shouldProcessNamespaces = false
            options.parserSettings.shouldReportNamespacePrefixes = false
            options.parserSettings.shouldResolveExternalEntities = false
            
            do{
                let xmlDoc = try AEXMLDocument(xml: xml, options: options)
                
                //print(xmlDoc.root["channel"]["item"]["title"].value!)
                if let title = xmlDoc.root["channel"]["item"].all{
                    for t in title{
                        if let myTitle = t["title"].value{
                            print(myTitle)
                        }
                    }
                }
                
            }catch{
                
            }
            
        }
        
    }
    
    

}

